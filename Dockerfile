FROM existenz/webstack:8.1

COPY --chown=php:nginx ./ /www
COPY --chown=php:nginx ./.env.example /www/.env

RUN find /www -type d -exec chmod -R 555 {} \; \
    && find /www -type f -exec chmod -R 444 {} \; \
    && apk -U --no-cache add \
    git \
    nodejs \
    npm \
    php81 \
    php81-ctype \
    php81-bcmath \
    php81-dom \
    php81-fileinfo \
    php81-gd \
    php81-iconv \
    php81-json \
    php81-mbstring \
    php81-openssl \
    php81-pdo \
    php81-pdo_mysql \
    php81-phar \
    php81-session \
    php81-simplexml \
    php81-tokenizer \
    bind-tools \
    php81-xml \
    php81-xmlreader \
    php81-xmlwriter \
    php81-zip \
    php81-curl

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /www

RUN chmod 777 /www/storage/ -R

RUN ln -s /usr/bin/php81 /usr/bin/php
RUN ["/usr/bin/composer", "config", "--global", "disable-tls", "true"]
RUN ["/usr/bin/composer", "install"]

RUN chmod 777 /www/ -R

# Frontend
RUN npm install --force
RUN npm run production

RUN chown php:nginx -R /www/
